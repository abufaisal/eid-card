# بطاقة عيد الأضحى

هذا المشروع يشمل صفحة ويب تقوم بإنشاء بطاقة عيد الأضحى المخصصة. يمكن للمستخدمين تخصيص البطاقة بإضافة أسمائهم واختيار الأبعاد.

## كيفية الاستخدام

1. قم بفتح الصفحة على GitLab: [صفحة بطاقة عيد الأضحى](https://gitlab.com/abufaisal/eid-card)
2. اتبع التعليمات على الصفحة لتخصيص البطاقة حسب رغبتك.
3. بعد التخصيص، اتبع الخطوات الموجودة في الصفحة لحفظ البطاقة ومشاركتها.

# Eid Al-Adha Card

This project includes a web page that creates a customized Eid Al-Adha card. Users can personalize the card by adding their names and choosing dimensions.

## How to Use

1. Open the page on GitLab: [Eid Al-Adha Card Page](https://gitlab.com/abufaisal/eid-card)
2. Follow the instructions on the page to customize the card according to your preferences.
3. After customization, follow the steps provided on the page to save and share the card.

## Note

You can also edit the README.md file to clarify any additional instructions or information about your project.
