	     var c1080 = document.getElementById("1080");
         var c1920 = document.getElementById("1920");
         var image1080 = document.getElementById("switchImage1");
         var image1920 = document.getElementById("switchImage2");
		 var flashText1920 = document.getElementById("flashText1920");
         var flashText1080 = document.getElementById("flashText1080");
		 image1080.addEventListener('click', () => {
			c1080.style.display = 'none';
			c1920.style.display = 'block';
			flashText1920.style.display = 'block';
			flashText1920.style.animation = "flashAnimation 1s forwards";
			flashText1080.style.display = "none";
			image1.checked = false;
			image2.checked = true;
         });
		 image1920.addEventListener('click', () => {
			c1080.style.display = 'block';
			c1920.style.display = 'none';
			flashText1080.style.display = 'block';
			flashText1080.style.animation = "flashAnimation 1s forwards";
			flashText1920.style.display = "none";
			image2.checked = false;
			image1.checked = true;
         });
		 	window.addEventListener("load", (event) => {
		    flashText1080.style.animation = "flashAnimation 1s forwards";
         });
saveButton.addEventListener('click', () => {
  const name = nameInput.value.trim();
  if (name.length > 0) {
    nameInput.value = '';
    downloadImage(name);
  }
});

function downloadImage(name) {
  const imageElement = image1.checked ? switchImage1 : switchImage2;
  const canvas = document.createElement('canvas');
  const context = canvas.getContext('2d');
  const image = new Image();

  image.onload = function () {
    canvas.width = image.width;
    canvas.height = image.height;
    context.drawImage(image, 0, 0);
    context.font = 'bold 46px ARBFONTS-BAHIJ-INSAN';
    context.fillStyle = '#ffffff';
    context.textAlign = 'left';
    if (image1.checked) {
        context.font = 'bold 46px ARBFONTS-BAHIJ-INSAN';
        context.textAlign = 'left';
    context.fillText(name, canvas.width / 2 - 400 , canvas.height - 170);
    } else {
    context.fillText(name, canvas.width / 2 - 330 , canvas.height - 340);
    }
    const downloadLink = document.createElement('a');
    downloadLink.href = canvas.toDataURL();
    downloadLink.download = 'eid_card.png';
    downloadLink.click();
  };

  image.src = imageElement.src;
}